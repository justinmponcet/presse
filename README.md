# Analyse de presse

Collection de scripts permettant une analyse de la presse. Scripts développé initialement pour le travail de thèse (2019-2023).

## Euro2CSV.py

Script Python pour convertir un export HTML Europress en CSV.Ce fichier peut être importé dans PressDB

## AnalysePresse.rmd

Notebook R pour une analyse statistique de la presse.


