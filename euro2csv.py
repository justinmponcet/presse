#!/usr/bin/python3
# coding: utf-8
# Tout droits réservés JMP 2021
# D'après les scripts de Léo Dumont https://framagit.org/leodumont/EuropresseToLexico et de https://quanti.hypotheses.org/1416 et http://www.iramuteq.org/git?p=iramuteq;a=blob_plain;f=parse_europress.py;hb=HEAD
# utilisation : python3 euro2csv.py
# Merci à Nicolas C. pour les conseils concernant Python

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from bs4 import BeautifulSoup
import re
import calendar
import locale
from lxml import etree
import string
import json
import csv
import sys

def html_parser(source):
    html = open(source, "r")
    content = html.read()
    soup = BeautifulSoup(content, "lxml")
    exported_articles = []

    articles = soup.findAll("article")
    for a in articles:
        article = {}

        journal = a.find("span", {"class", "DocPublicationName"})
        if journal is not None:
            journal_str = journal.text
            journal_str = journal_str.strip()
            journal_str = journal_str.replace(' ', '_')
            journal_str = journal_str.replace('(','')
            journal_str = journal_str.replace(')','')
            journal_str = journal_str.replace('-','')
            journal_str = journal_str.replace('.','')
            journal_str = journal_str.replace('/','')
            journal_str = journal_str.replace("'",'')
            journal_str = journal_str.replace(';', '')
            journal_str = journal_str.replace(':', '')
            journal_str = journal_str.replace(u'·','')
            journal_str = journal_str.lower()
            journal_str = journal_str.replace(' ', '-')
            journal_str = journal_str.split(',')[0]
            journal_str = journal_str.replace('_site_web', '')
            journal_str = journal_str.replace('__toutes_éditions', '')
            journal_str = journal_str.replace('__toutes', '')
            journal_str = journal_str.replace('le_parisien__paris', 'le_parisien')
            journal_str = journal_str.replace('le_mondefr', 'le_monde')
            journal_str = journal_str.replace('le_parisienfr', 'le_parisien')
            journal_str = journal_str.replace('le_moniteur_des_travaux_publics_et_du_bâtiment', 'le_moniteur')
            journal_str = journal_str.replace('le_pointfr', 'le_point')
            journal_str = journal_str.replace('les_echos__avis_financiers_français', 'les_echos')
            journal_str = journal_str.replace('libération__quotidien_premiere_edition', 'libération')
            journal_str = journal_str.replace('actufr_réf__actu_fr', 'actufr')
            journal_str = journal_str.replace('afp_doc', 'afp')
            journal_str = journal_str.replace('afp_infos_françaises', 'afp')
            journal_str = journal_str.replace('afp_infos_mondiales', 'afp')
            journal_str = journal_str.replace('afp__journal_internet_afp_français', 'afp')
            journal_str = journal_str.replace('alternatives_économiques_blogue_réf__alternatives_économiques_blogs', 'alternatives_économiques_blogs')
            journal_str = journal_str.replace('aujourdhui_en_france__edition_principale', 'aujourdhui_en_france')
            journal_str = journal_str.replace('boursorama_réf__boursorama', 'boursorama')
            journal_str = journal_str.replace('conseil_dorientation_des_retraites_réf__conseil_dorientation_des_retraites', 'conseil_dorientation_des_retraites')
            journal_str = journal_str.replace('france_assos_santé_réf__france_assos_santé', 'france_assos_santé')
            journal_str = journal_str.replace('francetv_info_réf__france_tv_info', 'francetv_info')
            journal_str = journal_str.replace('investirle_journal_des_finances', 'investir')
            journal_str = journal_str.replace('nord_éclair_réf', 'nord_éclair')
            journal_str = journal_str.replace('radio_france_internationale_français site_web_réf__rfi__radio_france_internationale', 'radio_france_internationale')
            journal_str = journal_str.replace('yahoo!_finance_france_réf__yahoo!_finance_fr', 'yahoo!_france')
            journal_str = journal_str.replace('yahoo!_france_réf__yahoo_france', 'yahoo!_france')
            article["journal"] = journal_str
        else:
            article["journal"] = "NA"

        date_str = a.find("span", {"class", "DocHeader"})
        if date_str is not None:
            date = re.search("(\d{1,2}) (\w+) (\d{4})", str(date_str))
            try:
                day = date.group(1)
                day = "{num:02d}".format(num=int(day))
                month = date.group(2)
                year = date.group(3)
                abbr_to_num = {name: num for num, name in enumerate(calendar.month_name) if num}
                month = abbr_to_num[month]
                month = "{num:02d}".format(num=month)
                good_date = "{}-{}-{}".format(year, month, day)
                article["date"] = good_date
                article["year"] = year
                article["month"] = "{}-{}".format(year, month)
            except:
                 article["date"] = "0000-00-00"
                 article["year"] = "0000"
                 article["month"] = "0000-00"
        else:
            article["date"] = "NA"

        article_title = a.find("p", {"class", "titreArticleVisu"})
        if article_title is not None:
            for c in string.punctuation:
                title = article_title.text.replace(c, "")
            title = title.replace("*", "")
            title = title.replace("\"", "")
            article["title"] = title
        else:
            article["title"] = "NA"


        author = a.find("div", {"class", "docAuthors"})
        if author is not None:
            author = author.text
            author = author.replace(" ", "-")
            author = author.replace("*", "")
            article["author"] = author
        else:
            article["author"] = "NA"

        content = a.find("div", {"class", "docOcurrContainer"})
        if content is not None:
            article["content"] = content.text.strip()
        else:
            article["content"] = "NA"

        exported_articles.append(article)

    return exported_articles

def export_csv(articles, output):
    with open(output, "w") as csv_file:
        fieldnames = ['date', 'year', 'month', 'journal', 'category', 'title', 'content', 'source']
        wr = csv.DictWriter(csv_file, fieldnames=fieldnames)
        wr.writeheader()
        for a in articles:
            wr.writerow({'date': a["date"], 'year': a['year'], 'month': a['month'], 'journal': a["journal"], 'category' : '', 'title' : a["title"], 'content': a["content"], 'source': 'Europresse'})
    return print("Corpus exporté au format CSV !");

def export_iramuteq(articles, output):
    with open(output, "w") as iramuteq_file:
        for a in articles:
                iramuteq_file.write("****"+" *date_"+a["date"] + " *year_"+a["year"] + " *month_"+a["month"] + " *journal_"+a["journal"] + "\n" + a["title"] + " " + a["content"] + "\n\n")
    return print("Corpus exporté au format Iramuteq !");


def gui_export_iramuteq(button):
    input_html = button_file.get_filename()
    output_iramuteq = button_file.get_filename()+"_iramuteq.txt"
    export_iramuteq(html_parser(input_html), output_iramuteq)
    button_iramuteq.set_label('Corpus exporté au format Iramuteq !')

def gui_export_file_csv(button): # Rechercher comment récupérer le filename
    input_html = button_file.get_filename()
    output_csv = button_file.get_filename()+".csv"
    export_csv(html_parser(input_html), output_csv)
    button_csv.set_label('Corpus exporté au format CSV !')

def gui_file_changed(button_file):
    print("File selected: %s" % button_file.get_filename())
    button_csv.set_label('Convertir en CSV')
    button_iramuteq.set_label('Convertir en Iramuteq')

if __name__ == "__main__":
#### GUI ######
   window = Gtk.Window()
   window.set_title("Europresse convertissor")
   window.set_border_width(10)
   window.connect('delete-event', Gtk.main_quit)


   box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)

   button_file = Gtk.FileChooserButton(title="FileChooserButton")
   filename = button_file.get_filename()

   button_csv = Gtk.Button(label='Convertir en CSV')
   button_iramuteq = Gtk.Button(label='Convertir en Iramuteq')
   button_quit = Gtk.Button(label='Quitter')

   box.pack_start(button_file, True, True, 0)
   box.pack_start(button_csv, True, True, 0)
   box.pack_start(button_iramuteq, True, True, 0)
   box.pack_start(button_quit, True, True, 0)
   window.add(box)

   button_file.connect("file-set", gui_file_changed)
   button_csv.connect('clicked', gui_export_file_csv)
   button_iramuteq.connect('clicked', gui_export_iramuteq)
   button_quit.connect('clicked', Gtk.main_quit)


   window.show_all()
   Gtk.main()
#### CLI #####
#   locale.setlocale(locale.LC_ALL, "fr_FR.utf8")
#   input_html = sys.argv[1]
#   output_csv = sys.argv[1]+".csv"
#   output_iramuteq = sys.argv[1]+"_iramuteq.txt"
#   export_iramuteq(html_parser(input_html), output_iramuteq)
#   export_csv(html_parser(input_html), output_csv)
